//
//  NetworkCall.m
//  ImageListView
//

#import "NetworkCall.h"
#import "ImageSearchResult.h"

@implementation NetworkCall

#pragma mark - Singleton Method

+ (NetworkCall *)sharedInstance {
    static NetworkCall *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

#pragma mark - Network Call Method

- (void)getGoogleImageForSearchQuery: (NSString *)searchQuery returnBlock:( void(^)(NSArray *dataArray, NSError *connectionError))returnBlock {
    
    NSURL * searchUrl = [NSURL URLWithString:[NSString stringWithFormat:@"https://ajax.googleapis.com/ajax/services/search/images?v=1.0&q=%@",searchQuery]];
    NSMutableURLRequest * searchRequest = [[NSMutableURLRequest alloc] initWithURL:searchUrl];
    
    [searchRequest setHTTPMethod:@"GET"];
    
    [NSURLConnection sendAsynchronousRequest:searchRequest queue:[NSOperationQueue new] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        
        returnBlock([self parseResponse:data], connectionError);
    }];
}

- (NSArray *)parseResponse:(NSData *)data {
    NSError *error = nil;
    NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    NSMutableArray *dictArray = [NSMutableArray new];
    NSMutableArray *searchResultArray = [NSMutableArray new];
    
    if ([dictionary hasValueForKey:@"responseData"]) {
        
        NSDictionary *reponseDict = [dictionary valueForKey:@"responseData"];
        if ([reponseDict hasValueForKey:@"results"]) {
            dictArray = [reponseDict valueForKey:@"results"];
        }
        
        for (NSDictionary *dict in dictArray) {
            ImageSearchResult *imageSearchResult = [ImageSearchResult parseJsonResponseForDict:dict];
            [searchResultArray addObject:imageSearchResult];
        }
    }
    
    return searchResultArray;
}

@end
