//
//  NetworkCall.h
//  ImageListView
//

#import <Foundation/Foundation.h>

@interface NetworkCall : NSObject

+ (NetworkCall *)sharedInstance;
- (void)getGoogleImageForSearchQuery: (NSString *)searchQuery returnBlock:( void(^)(NSArray *dataArray, NSError *connectionError))returnBlock;

@end
