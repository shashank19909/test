//
//  ImageSearchResult.m
//  ImageListView
//

#import "ImageSearchResult.h"

@implementation ImageSearchResult

+ (ImageSearchResult *)parseJsonResponseForDict:(NSDictionary *)responseDict {
    ImageSearchResult *imageSearchResult = [ImageSearchResult new];
    if ([responseDict hasValueForKey:@"tbUrl"]) {
        imageSearchResult.thumbnailImageUrl = [NSURL URLWithString:[responseDict valueForKey:@"tbUrl"]];
    }
    if ([responseDict hasValueForKey:@"url"]) {
        imageSearchResult.thumbnailImageUrl = [NSURL URLWithString:[responseDict valueForKey:@"url"]];
    }
    return imageSearchResult;
}

@end
