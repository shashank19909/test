//
//  ImageSearchResult.h
//  ImageListView
//

#import <Foundation/Foundation.h>

@interface ImageSearchResult : NSObject

@property (nonatomic, strong) NSURL *thumbnailImageUrl;
@property (nonatomic, strong) NSURL *imageUrl;

+ (ImageSearchResult *)parseJsonResponseForDict:(NSDictionary *)responseDict;

@end
