//
//  SearchResultCell.m
//  ImageListView
//
//  Created by Sumit on 26/07/15.
//  Copyright (c) 2015 Shashank. All rights reserved.
//

#import "SearchResultCell.h"
#import "ImageSearchResult.h"

@interface SearchResultCell ()

@property (nonatomic, weak) IBOutlet UIImageView *searchResultImageView;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicatorView;

@end

@implementation SearchResultCell

#pragma mark - Awake Method

- (void)awakeFromNib {
    // Initialization code
}

#pragma mark - Public Method

- (void)assignDataToCell:(ImageSearchResult *)imageSearchResult {
    [self.activityIndicatorView startAnimating];
    [self.searchResultImageView sd_setImageWithURL:imageSearchResult.thumbnailImageUrl completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        [self.activityIndicatorView stopAnimating];
    }];
}

#pragma mark - Selection Method

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
