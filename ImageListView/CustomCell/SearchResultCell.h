//
//  SearchResultCell.h
//  ImageListView
//
//  Created by Sumit on 26/07/15.
//  Copyright (c) 2015 Shashank. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ImageSearchResult;

@interface SearchResultCell : UITableViewCell

- (void)assignDataToCell:(ImageSearchResult *)imageSearchResult;

@end
