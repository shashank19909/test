//
//  SearchResultsViewController.m
//  ImageListView
//

#import "SearchResultsViewController.h"
#import "ImageSearchResult.h"
#import "NetworkCall.h"
#import "SearchResultCell.h"

@interface SearchResultsViewController () <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate> {
    BOOL isApiCallInProgress;
}

@property (nonatomic, weak) IBOutlet UITableView *searchResultTableView;
@property (nonatomic, weak) IBOutlet UITextField *searchTextField;
@property (nonatomic, weak) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (nonatomic, strong) NSMutableArray *searchResults;

@end

@implementation SearchResultsViewController

#pragma mark - View LifeCycle

- (void)viewDidLoad {
    [super viewDidLoad];
    self.searchTextField.delegate = self;
    // Do any additional setup after loading the view.
}

#pragma mark - Private Method

- (void)reloadResponse:(NSArray *)responseArray {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.activityIndicator stopAnimating];
        if (responseArray.count > 0) {
            self.searchResults = [[NSMutableArray alloc] initWithArray:responseArray];
        }
        [self.searchResultTableView reloadData];
    });
}

#pragma mark - TableView DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.searchResults.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SearchResultCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SearchResultCell"];
    [cell assignDataToCell:[self.searchResults objectAtIndex:indexPath.row]];
    return cell;
}

#pragma mark - IBAction

- (IBAction)searchButtonTapped:(id)sender {
    [self searchForKeyword:self.searchTextField.text];
    [self.searchTextField resignFirstResponder];
}

#pragma mark - API

- (void)searchForKeyword:(NSString *)searchKeyWord {
    __unsafe_unretained typeof(self) weakSelf = self;
    [self.activityIndicator startAnimating];
    
    [[NetworkCall sharedInstance] getGoogleImageForSearchQuery:searchKeyWord returnBlock:^(NSArray *dataArray, NSError *connectionError) {
        [weakSelf reloadResponse:dataArray];
    }];
    
}

#pragma mark TextField Delegate Methods
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (![textField.text isEqualToString:@""]) {
       [self searchForKeyword:textField.text];
    }
    [textField resignFirstResponder];

    return YES;
}


#pragma Memory Management

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
