//
//  NSDictionary+hasValueForKey.h

#import <Foundation/Foundation.h>

@interface NSDictionary_hasValueForKey : NSDictionary



@end

@interface NSDictionary (HasValueForKey)

- (BOOL)hasValueForKey:(NSString *)key;

@end
